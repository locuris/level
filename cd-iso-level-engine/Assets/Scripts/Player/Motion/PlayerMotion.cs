﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for controling:
/// * Player Movement
/// * Player Movement Animation
/// </summary>
public class PlayerMotion : MonoBehaviour
{
    /// <summary>
    /// Toggle for user controlled player movement to be on or off:
    /// TRUE = ON
    /// FALSE = OFF
    /// </summary>
    public bool userMovementEnabled;

    public bool rigidMovementMode;
    
    /// <summary>
    /// Float value for Speed.
    /// Should be greater than zero
    /// </summary>
    public float speed;

    public bool noControler;

    public float targetOffset;
    public GameObject realPosition;

    private Rigidbody2D rbody;

    /// <summary>
    /// Make sure that the Player object (this) has the PlayerAnimation script attached
    /// </summary>
    private PlayerAnimation animator;

    public event EventHandler ArrivedAtTarget;

    /// <summary>
    /// Vector2 position for moving the character without user input
    /// </summary>
    private Vector2 targetPosition;

    private bool movingToTarget;
    private bool overrideUserMovement;

    private Vector2 PlayerPosition
    {
        get
        {
            return realPosition.transform.position;
        }
    }

    /// <summary>
    /// MonoBehaviour method for preparing object before scene frame processing starts.
    /// * Finding the Animator script attached to this gameobject
    /// </summary>
    private void Awake()
    {
        animator = GetComponent<PlayerAnimation>();
        rbody = GetComponent<Rigidbody2D>();
        if (animator == null)
            Debug.LogError("No animator attached to the Player object.");
        if (speed <= 0)
            Debug.LogWarning("PlayerMotion speed set to zero or less: " + speed);
    }

    /// <summary>
    /// MonoBehaviour method for checking behavour on each frame.
    /// * Listens for User input for player motion if movementEnabled is TRUE
    /// </summary>
    void FixedUpdate()
    {
        if (userMovementEnabled && !overrideUserMovement)
            HandleMovement();
        else if (movingToTarget)
            MoveToTarget();
    }

    private void HandleMovement()
    {
        float x = 0;
        float y = 0;

        if(!noControler)
        {
            x = Input.GetAxis("Horizontal");
            y = Input.GetAxis("Vertical");
        }
        else if(noControler)
        {
            x = Input.GetKey(KeyCode.D) ? 1 : Input.GetKey(KeyCode.A) ? -1 : 0;
            y = Input.GetKey(KeyCode.W) ? 1 : Input.GetKey(KeyCode.S) ? -1 : 0;
        }
        MovePlayer(x, y);
    }

    private void MovePlayer(float x, float y)
    {
        if (rigidMovementMode)
            HandleMovementRigid(x, y);
        else HandleMovementTranslate(x, y);

        animator.SetDirection(x, y);
    }

    private void MoveToTarget()
    {
        if(Math.Abs(Vector2.Distance(PlayerPosition, targetPosition)) <= targetOffset)
        {
            Debug.Log("Player arrived at target");
            movingToTarget = false;
            overrideUserMovement = false;

            EventHandler handler = ArrivedAtTarget;
            handler?.Invoke(this, new EventArgs());

            return;
        }
        Vector2 direction = targetPosition - PlayerPosition;
        Debug.Log("Moving player in direction: " + direction);
        
        direction.Normalize();

        MovePlayer(direction.x, direction.y);

    }

    /// <summary>
    /// Method for listening for user input using the Horizontal and Vertical input axis
    /// Will set the motion of x and y and pass those values to the animator to transition
    /// between direction clips (Up, Down, Left, Right, and Diagonals).
    /// </summary>
    private void HandleMovementTranslate(float x, float y)
    {
        if (x != 0 || y != 0)
        {
            Vector2 movement = new Vector2(x, y);
            gameObject.transform.Translate(movement.normalized * speed * Time.fixedDeltaTime);
            Moving = true;
        }
        else Moving = false;                
    }

    private void HandleMovementRigid(float x, float y)
    {
        if(x == 0 && y == 0)
        {
            Moving = false;
            return;
        }
        Moving = true;

        Vector2 currentPos = rbody.position;
        Vector2 inputVector = new Vector2(x, y);

        inputVector = Vector2.ClampMagnitude(inputVector, 1);

        Vector2 movement = inputVector * speed;
        Vector2 newPos = currentPos + movement * Time.fixedDeltaTime;

        rbody.MovePosition(newPos);
    }    

    public void SetTarget(Vector2 target)
    {
        Debug.Log("Setting player motion target to: " + target);
        targetPosition = target;
        overrideUserMovement = true;
        movingToTarget = true;
    }

    public void ToggleUserMovement()
    {
        userMovementEnabled = !userMovementEnabled;
        Debug.Log("Player Movement Toggled: "+userMovementEnabled);
    }

    public void EnableMovement()
    {
        overrideUserMovement = false;
    }    

    public void DisableMovement()
    {
        overrideUserMovement = true;
    }

    public static bool Moving;

}
