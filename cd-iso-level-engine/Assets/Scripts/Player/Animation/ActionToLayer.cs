﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class ActionToLayer
{
    public AnimationEventType animationEvent;
    public int layerName;
}
