﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Net.Security;
using System.Runtime.CompilerServices;
using UnityEngine;

public enum PlayerDirection { ANY, N, NE, E, SE, S, SW, W, NW }

public enum AnimationEventType { DEFAULT, PICK_UP_BUCKET, REFILL, POUR, SIT_DOWN, OPEN_HATCH, PLACE_OBJECT }

public delegate void GenericAnimationEvent(AnimationEventType animation);

public class PlayerAnimation : MonoBehaviour
{
    /// <summary>
    /// Toggle for ignoring animation blend.
    /// If TRUE there will be no transition between different direction animations
    /// it will just move between animations from one frame to the next.
    /// If FALSE it will transition between them creating intermediate animations
    /// between one direction and the next.
    /// </summary>
    public bool overrideBlend;

    public int idleStates;

    public event GenericAnimationEvent genericAnimationEvent;
    public event GenericAnimationEvent animFinishedEvent;

    private Animator animator;

    private Vector2 lastDirection = new Vector2(0, -1);

    // Animator params
    private const string X_DIRECTION_PARAM = "X";
    private const string Y_DIRECTION_PARAM = "Y";
    private const string WALKING_PARAM = "walking";
    private const string IDLE_STATE_PARAM = "idle";
    private const string ACTION_TRIGGER_PARAM = "actionT";
    private const string ACTION_BOOL_PARAM = "actionB";
    private const string ACTION_INT_PARAM = "action";

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void SetDirection(float x, float y)
    {
        if (overrideBlend)
        {
            if (x < 0)
                x = -1;
            else if (x > 0)
                x = 1;
            if (y < 0)
                y = -1;
            else if (y > 0)
                y = 1;
        }

        Vector2 direction = new Vector2(x, y);

        if (direction.magnitude < 0.01f)
        {
            SetWalkingState(lastDirection, false);
        }
        else
        {
            lastDirection = direction;
            SetWalkingState(direction, true);
        }
    }

    public void ForceDirection(PlayerDirection direction)
    {
        Vector2 vectorDirection = DirectionToVector2(direction);
        lastDirection = vectorDirection;
        SetWalkingState(
            vectorDirection, false);
    }

    public void TriggerAction(AnimationEventType animEvent)
    {
        animator.SetInteger(ACTION_INT_PARAM, (int)animEvent);
        animator.SetTrigger(ACTION_TRIGGER_PARAM);
    }

    public void ToggleAction(AnimationEventType animEvent)
    {
        animator.SetInteger(ACTION_INT_PARAM, (int)animEvent);
        animator.SetBool(ACTION_BOOL_PARAM, !animator.GetBool(ACTION_BOOL_PARAM));
    }

    private void SetWalkingState(Vector2 direction, bool walking)
    {
        if (!walking)
            animator.SetInteger(IDLE_STATE_PARAM, Random.Range(0, idleStates));
        animator.SetBool(WALKING_PARAM, walking);
        animator.SetFloat(X_DIRECTION_PARAM, direction.x);
        animator.SetFloat(Y_DIRECTION_PARAM, direction.y);
    }

    private Vector2 DirectionToVector2(PlayerDirection direction)
    {
        int x = 0;
        int y = 0;
        switch (direction)
        {
            case PlayerDirection.N:
                y = 1;
                break;
            case PlayerDirection.NE:
                x = 1;
                y = 1;
                break;
            case PlayerDirection.E:
                x = 1;
                break;
            case PlayerDirection.SE:
                x = 1;
                y = -1;
                break;
            case PlayerDirection.S:
                y = -1;
                break;
            case PlayerDirection.SW:
                x = -1;
                y = -1;
                break;
            case PlayerDirection.W:
                x = -1;
                break;
            case PlayerDirection.NW:
                x = -1;
                y = 1;
                break;
        }
        return new Vector2(x, y);
    }

    public void GenericAnimationEvent(AnimationEventType animation)
    {
        genericAnimationEvent?.Invoke(animation);
    }

    public void AnimationFinishedEvent(AnimationEventType animation)
    {
        animFinishedEvent?.Invoke(animation);
    }

    
}
