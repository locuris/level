﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{

    public InputAction dropObjectAction;
    public Vector2[] dropObjectOffset;

    private PlayerAnimation playerAnim;
    private PlayerMotion playerMotion;

    private List<DirectionalCollider> interactableColliders;
    private DirectionalCollider target;
    
    private const string INTERACTABLE_TAG = "Interactable";

    private GameObject carriedObject;

    private DirectionalCollider ClosestInteractable
    {
        get
        {
            float distance = 999f;
            DirectionalCollider interactable = null;
            foreach(DirectionalCollider dc in interactableColliders)
            {
                if (dc == null)
                    continue;

                float newDist = Vector2.Distance(transform.position, dc.transform.position);

                if (newDist < distance)
                {
                    interactable = dc;
                    distance = newDist;
                }
            }

            return interactable;
        }
    }

    private void Awake()
    {
        playerAnim = GetComponentInParent<PlayerAnimation>();
        if (playerAnim == null)
            Debug.LogError("Missing PlayerAnimation script from player object");

        playerAnim.genericAnimationEvent += GenericAnimationEvent;
        playerAnim.animFinishedEvent += AnimationFinishedEvent;

        playerMotion = GetComponentInParent<PlayerMotion>();
        if(playerMotion == null)
            Debug.LogError("Missing PlayerMotion script from player object");

        playerMotion.ArrivedAtTarget += PlayerArrivedAtTarget;

        interactableColliders = new List<DirectionalCollider>();

    }

    private void Update()
    {
        if(ClosestInteractable != null)
        {
            if(InputConfig.ActionKeyPressed(ClosestInteractable.GetInteractableObject.interactionInput))
            {
                if (!ClosestInteractable.GetInteractableObject.CheckStateCondition())
                    return;

                if (ClosestInteractable.GetInteractableObject.singleInteraction && ClosestInteractable.GetInteractableObject.Interacted)
                    return;

                target = ClosestInteractable;

                target.GetInteractableObject.DisableCollider();

                playerMotion.SetTarget(target.transform.position);
            }
        }
        else
        {
            if (GameState.PlayerState.PlayerIsCarrying != CarriedObject.NOTHING 
                && InputConfig.ActionKeyPressed(dropObjectAction))
            {
                PlaceCarriedObject();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == INTERACTABLE_TAG)
        {
            DirectionalCollider dc = collision.gameObject.GetComponent<DirectionalCollider>();
            if(dc == null)
            {
                Debug.LogError("InteractableObject does not have DirectionalCollider configured! " + collision.name);
                return;
            }

            if (interactableColliders.Contains(dc))
                return;

            interactableColliders.Add(dc);
        }        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == INTERACTABLE_TAG)
        {
            DirectionalCollider dc = collision.gameObject.GetComponent<DirectionalCollider>();
            if (dc == null)
            {
                Debug.LogError("InteractableObject does not have DirectionalCollider configured! " + collision.name);
                return;
            }

            if (!interactableColliders.Contains(dc))
                return;

            interactableColliders.Remove(dc);
        }
    }

    private void PlayerArrivedAtTarget(object sender, EventArgs e)
    {
        InteractableObject io = target.GetInteractableObject;
        playerAnim.ForceDirection(target.direction);

        if(io.triggerObjectAction && io.triggerPlayerAction)
        {
            io.InvokeAnimationAction();
            playerAnim.TriggerAction(io.animationAction);
        }
        else if (io.triggerPlayerAction)
        {
            playerAnim.TriggerAction(io.animationAction);
        }
        else if(io.triggerObjectAction)
        {
            io.InvokeAnimationAction();
        }

        if(io.togglePlayerAction)
        {
            playerAnim.ToggleAction(io.animationAction);
        }

        if (io.toggleMovement)
            playerMotion.ToggleUserMovement();

        if (io.freezeMovement)
            playerMotion.DisableMovement();

        io.UpdateState();

        io.Interacted = true;
    }

    private void GenericAnimationEvent(AnimationEventType animation)
    {
        if (target != null)
        {
            InteractableObject io = target.GetInteractableObject;
            if (io.hideObject)
                io.DisableSprite();
            if (!io.hideObject)
                io.EnableCollider();
            if (io.pickedUpObject != PlaceablePrefab.NONE)
                carriedObject = PrefabManager.GetPrefab(io.pickedUpObject);
        }
        else if(carriedObject != null)
        {
            Instantiate(carriedObject, transform.position, transform.rotation);
            carriedObject = null;
            GameState.PlayerState.PlayerIsCarrying = CarriedObject.NOTHING;
        }
    }

    private void AnimationFinishedEvent(AnimationEventType animation)
    {
        if (target != null)
        {
            InteractableObject io = target.GetInteractableObject;
            if (io.freezeMovement)
                playerMotion.EnableMovement();
            io.EndInteraction();
            target = null;
        }
    }

    private void PlaceCarriedObject()
    {
        if(target == null && carriedObject != null)
            playerAnim.TriggerAction(AnimationEventType.PLACE_OBJECT);
    }

}
