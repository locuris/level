﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class InputControl
{
    public InputAction action;
    public KeyCode keyCode;
    public Sprite uiIcon;
    public Sprite uiIconDown;
}
