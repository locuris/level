﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputAction { INTERACT, CANCEL }

public class InputConfig : MonoBehaviour
{
    public InputControl[] controls;

    public GameObject actionPromptPrefab;
    
    private void Awake()
    {
        DontDestroyOnLoad(this);

        actionPrompt = actionPromptPrefab;

        controlsMap = new Dictionary<InputAction, InputControl>();
        foreach (InputControl ic in controls)
        {
            if (controlsMap.ContainsKey(ic.action)) {
                Debug.LogWarning("Multiple keys mapped to action: " + ic.action.ToString());
                continue;
            }
            controlsMap.Add(ic.action, ic);
        }
    }

    private static Dictionary<InputAction, InputControl> controlsMap;
    private static GameObject actionPrompt;

    private static GameObject actionPromptObject;

    public static KeyCode ActionKey(InputAction action)
    {
        if(!controlsMap.ContainsKey(action))
        {
            Debug.LogError("No KeyCode mapped to this action: " + action.ToString());
            return KeyCode.None;
        }
        return controlsMap[action].keyCode;
    }

    public static Sprite ActionUIIcon(InputAction action)
    {
        if (!controlsMap.ContainsKey(action))
        {
            Debug.LogError("No UI Icon mapped to this action: " + action.ToString());
            return null;
        }
        return controlsMap[action].uiIcon;
    }

    public static Sprite ActionUIIconDown(InputAction action)
    {
        if (!controlsMap.ContainsKey(action))
        {
            Debug.LogError("No UI Icon Down mapped to this action: " + action.ToString());
            return null;
        }
        return controlsMap[action].uiIconDown;
    }

    public static bool ActionKeyPressed(InputAction action)
    {
        if (!controlsMap.ContainsKey(action))
        {
            Debug.LogError("No KeyCode mapped to this action: " + action.ToString());
            return false;
        }
        return Input.GetKeyDown(controlsMap[action].keyCode);
    }

    public static void DisplayActionPrompt(Vector2 positon, InputAction action)
    {
        actionPromptObject = Instantiate(actionPrompt);
        actionPromptObject.GetComponent<ActionPrompt>().DisplayIcon(positon, controlsMap[action].uiIcon);

    }

}
