﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Video;

public class InteractableObject : MonoBehaviour
{
    public AnimationEventType animationAction;
    public InputAction interactionInput;
    public bool triggerPlayerAction;
    public bool togglePlayerAction;
    public bool triggerObjectAction;
    public float objectActionDelay;
    public bool hideObject;
    public bool destroyObjectAfterInteraction;
    public bool singleInteraction;
    public bool toggleMovement;
    public bool freezeMovement;
    public PlaceablePrefab pickedUpObject;

    [TextArea(1, 5)]
    public string setState;
    [TextArea(1, 5)]
    public string checkState;

    private Animator animator;

    public bool Interacted { get; set; }

    private const string ACTION_TRIGGER_PARAM = "actionT";

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void InvokeAnimationAction()
    {
        Invoke("TriggerAnimationAction", objectActionDelay);
    }

    public void EndInteraction() 
    {
        if (destroyObjectAfterInteraction)
            Destroy(gameObject);
    }

    public void DisableCollider()
    {
        GetComponent<Collider2D>().enabled = false;
    }

    public void EnableCollider()
    {
        GetComponent<Collider2D>().enabled = true;
    }

    public void DisableSprite()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    public bool CheckStateCondition()
    {
        if (checkState == "")
            return true;

        string[] conditions = checkState.Split(
            new string[] { Environment.NewLine }, 
            StringSplitOptions.RemoveEmptyEntries);
                
        foreach(string con in conditions)
        {
            string[] conComps = con.Split('=');

            if(conComps.Length != 2)
            {
                Debug.LogError("Condition does not contain the correct amount of components: " + name + " / " + con);
                continue;
            }

            if (!GameState.CheckStringCondition(conComps[0], conComps[1]))
                return false;
        }

        return true;
    }

    public void UpdateState()
    {
        if (setState == "")
            return;

        string[] states = setState.Split(
            new string[] { Environment.NewLine },
            StringSplitOptions.RemoveEmptyEntries);

        foreach(string state in states)
        {
            string[] stateComps = state.Split('=');

            if (stateComps.Length != 2)
            {
                Debug.LogError("State does not contain the correct amount of components: " + name + " / " + state);
                continue;
            }

            GameState.UpdateStateFromString(stateComps[0], stateComps[1]);
        }
    }

    private void TriggerAnimationAction()
    {
        animator.SetTrigger(ACTION_TRIGGER_PARAM);
    }
}
