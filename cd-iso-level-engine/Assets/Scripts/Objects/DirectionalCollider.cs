﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionalCollider : MonoBehaviour
{
    public PlayerDirection direction;

    private InteractableObject interactableObject;

    private void Awake()
    {
        interactableObject = GetComponentInParent<InteractableObject>();
        if (interactableObject == null)
            Debug.LogError("Directional Collider does not have IneractableObject in parent. "+transform.parent.name);
    }

    public InteractableObject GetInteractableObject
    {
        get
        {
            return interactableObject;
        }
    }

}
