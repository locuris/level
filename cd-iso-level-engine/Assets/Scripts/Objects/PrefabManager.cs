﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlaceablePrefab { NONE, SPOUT }

public class PrefabManager : MonoBehaviour
{
    public GameObject[] placeablePrefabs;
    // Start is called before the first frame update
    void Start()
    {
        prefabs = new Dictionary<PlaceablePrefab, GameObject>();
        prefabs.Add(PlaceablePrefab.SPOUT, placeablePrefabs[0]);
    }

    private static Dictionary<PlaceablePrefab, GameObject> prefabs;

    public static GameObject GetPrefab(PlaceablePrefab prefab)
    {
        return prefabs[prefab];
    }
}
