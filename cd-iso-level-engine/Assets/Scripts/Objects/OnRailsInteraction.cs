﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class OnRailsInteraction : MonoBehaviour
{
    public GameObject ORPlayerObject;    

    public bool transitionToMovement;

    public Animator animator;

    public GameObject playerObject;

    public abstract void StartInteraction();

    public abstract void FinishInteraction();

    public void TransitionToMovement()
    {
        ORPlayerObject.SetActive(false);
        playerObject.SetActive(true);
    }

}
