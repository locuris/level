﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionPrompt : MonoBehaviour
{
    private SpriteRenderer spriteR;

    private void Awake()
    {
        spriteR = GetComponent<SpriteRenderer>();
    }

    public void DisplayIcon(Vector2 position, Sprite sprite)
    {
        transform.position = position;
        spriteR.sprite = sprite;
    }
}
