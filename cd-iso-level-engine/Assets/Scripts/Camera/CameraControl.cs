﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class for controling Camera Motion.
/// Current Options:
/// * Follow Player or Not
/// * Deadspace when following Player.
/// </summary>
public class CameraControl : MonoBehaviour
{
    /// <summary>
    /// Toggle for following Player.
    /// If set to TRUE the Camera will follow the player else it will stay in
    /// it's set position
    /// </summary>
    public bool followPlayer;

    /// <summary>
    /// Deadspace value. When followPlayer is set it will use this value
    /// to determine the distance from the center of the screen the player
    /// can move before the camera starts following.
    /// </summary>
    public float deadspace;

    public float acceleration;
    public float accelerationMod;

    private float accelerationFactor;

    /// <summary>
    /// Player GameObject, with playerMotion script.
    /// </summary>
    private GameObject player;

    private bool bgBlack;
    
    /// <summary>
    /// MonoBehaviour method for preparing object before scene starts frame processing
    /// * Find Player object (tagged with "Player")
    /// * If followPlayer is true it will set start position of the camera to the players position
    /// </summary>
    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        if (followPlayer)
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y, transform.position.z);

    }

    /// <summary>
    /// Executes FollowPlayer method every frame if followPlayer is TRUE
    /// </summary>
    private void FixedUpdate()
    {
        if (followPlayer)
            FollowPlayer();
        if(Input.GetKeyDown(KeyCode.Tab))
        {
            Camera.main.backgroundColor = bgBlack ? Color.white : Color.black;
            bgBlack = !bgBlack;
        }
    }

    /// <summary>
    /// Determines the distance the camera should translate based on player position
    /// since last frame and deadspace value.
    /// </summary>
    private void FollowPlayer()
    {

        float xDiff = player.transform.position.x - transform.position.x;
        float yDiff = player.transform.position.y - transform.position.y;

        if (Math.Abs(xDiff) >= deadspace)
        {
            if (xDiff > 0)
                xDiff -= deadspace;
            else if (xDiff < 0)
                xDiff += deadspace;

            xDiff *= accelerationFactor;
        }
        else xDiff = 0;
        if (Math.Abs(yDiff) >= deadspace)
        {
            if (yDiff > 0)
                yDiff -= deadspace;
            else if (yDiff < 0)
                yDiff += deadspace;

            yDiff *= accelerationFactor;
        }
        else yDiff = 0;

        if (xDiff == 0 && yDiff == 0)
            accelerationFactor = acceleration;
        else if (accelerationFactor < 1)
        {
            accelerationFactor += Time.deltaTime * accelerationMod;
        }


        transform.Translate(new Vector3(xDiff, yDiff, 0));

    }    

}
