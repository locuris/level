﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraEffects : MonoBehaviour
{
    public float shakeDuration;
    public float shakeAmount;

    private Vector3 origPos;
    private bool shaking;
    private float shakeTime;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        if(Input.GetKeyDown(KeyCode.Alpha1) && !shaking)
        {
            shaking = true;
            shakeTime = shakeDuration;
            origPos = transform.position;
        }

        if (shaking)
            Shake();


        /*if (shakeOnStop)
        {
            if (shaken && PlayerMotion.Moving)
            {
                shaken = false;
                shakeTime = shakeDuration;
            }

            if (!shaken && !PlayerMotion.Moving)
                Shake();
        }*/
    }

    public void Shake()
    {
        if (shakeTime > 0)
        {
            Vector3 shakePos = origPos + UnityEngine.Random.insideUnitSphere * shakeAmount;
            shakePos.z = transform.position.z;

            transform.position = shakePos;

            shakeTime -= Time.deltaTime;
        }
        else
        {
            shakeTime = 0;
            transform.position = origPos;
            shaking = false;
        }
    }
}
