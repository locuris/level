﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class GameState
{
    public static PlayerState PlayerState = new PlayerState();

    public static bool CheckStringCondition(string obj, string val)
    {
        if(obj == "PlayerCarrying")
        {
            return PlayerState.PlayerIsCarrying.ToString() == val;
        }

        return false;
    }

    public static void UpdateStateFromString(string obj, string val)
    {
        if (obj == "PlayerCarrying")
        {
            PlayerState.PlayerIsCarrying = (CarriedObject)Enum.Parse(typeof(CarriedObject), val, true);
        }
    }

}
