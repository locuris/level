﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bedroom : MonoBehaviour
{
    public OnRailsInteraction bedroomInteraction;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            bedroomInteraction.StartInteraction();
        }
    }
}
