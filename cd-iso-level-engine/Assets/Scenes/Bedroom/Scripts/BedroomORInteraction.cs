﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedroomORInteraction : OnRailsInteraction
{
    private int stage = 0;
    private bool playing;

    private const string ANIM_INT_PARAM = "stage";
    private const int STAGE_MAX = 3;

    private bool finishing;

    public override void FinishInteraction()
    {
        if (transitionToMovement)
            TransitionToMovement();
        playing = false;
    }

    public override void StartInteraction()
    {
        animator.SetInteger(ANIM_INT_PARAM, stage);
        playing = true;
        InputConfig.DisplayActionPrompt(new Vector2(-0.1f, -0.1f), InputAction.INTERACT);
    }

    private void Update()
    {
        if (playing && InputConfig.ActionKeyPressed(InputAction.INTERACT))
        {
            if (stage < STAGE_MAX)
            {
                stage++;
                animator.SetInteger(ANIM_INT_PARAM, stage);
            }
        }
        /*if (playing && !animator.GetCurrentAnimatorStateInfo(0).loop)
            Debug.Log("S: " + stage + " | " + animator.GetCurrentAnimatorStateInfo(0).normalizedTime);*/
        /*if (playing && stage == STAGE_MAX && animator.GetCurrentAnimatorStateInfo(0).IsName("Anm_05_Str_Boots_Stand") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
            Debug.Log("FINISHED! "+ animator.GetCurrentAnimatorStateInfo(0).normalizedTime);*/
        /*if (playing && stage == STAGE_MAX && animator.GetCurrentAnimatorStateInfo(0).IsName("Anm_05_Str_Boots_Stand") && !finishing)
            finishing = true;
        if (playing && stage == STAGE_MAX && !animator.GetCurrentAnimatorStateInfo(0).IsName("Anm_05_Str_Boots_Stand") && finishing)
        {
            finishing = false;
            FinishInteraction();
        }*/

    }
}
