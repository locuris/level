﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum MotionDirection
{
    WALK_UP_RIGHT,
    WALK_UP_LEFT,
    WALK_DOWN_LEFT,
    WALK_DOWN_RIGHT,
    WALK_RIGHT,
    WALK_LEFT,
    WALK_DOWN,
    WALK_UP,
    IDLE_REAR,
    IDLE_FRONT
}

public class PlayerMotion : MonoBehaviour
{
    public bool movementEnabled;
    public float speed;

    private const float SPEED_MODIFIER = 0.5f;
    private CharAnimController charAnimController;
    private bool lastWalkedUp;
    private MotionDirection lastDirection = MotionDirection.IDLE_FRONT;
    private Vector2 lastPosition;

    // Start is called before the first frame update
    void Start()
    {
        charAnimController = GetComponent<CharAnimController>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(movementEnabled)
        {
            HandleMovement();
            Debug.Log("SPEED: " +Vector2.Distance(lastPosition, transform.position));
            lastPosition = transform.position;
        }
    }    

    private void HandleMovement()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        if(x != 0 || y != 0)
        {
            /*if(x != 0 && y != 0)
            {
                x *= 0.74f;
                y *= 0.74f;
            }*/
            Vector2 movement = new Vector2(x, y);
            /*gameObject.transform.Translate(new Vector3(
                x * speed * SPEED_MODIFIER, 
                y * speed * SPEED_MODIFIER,
                0));*/
            gameObject.transform.Translate(movement.normalized * speed * Time.deltaTime * SPEED_MODIFIER);
            if (y > 0)
                lastWalkedUp = true;
            else lastWalkedUp = false;
        }
        HandleAnimation(x, y);
    }

    private void HandleAnimation(float x, float y)
    {
        MotionDirection newDirection = MotionDirection.IDLE_FRONT;
        if (x > 0 && y > 0)
            newDirection = MotionDirection.WALK_UP_RIGHT;
        else if (x < 0 && y > 0)
            newDirection = MotionDirection.WALK_UP_LEFT;
        else if (x < 0 && y < 0)
            newDirection = MotionDirection.WALK_DOWN_LEFT;
        else if (x > 0 && y < 0)
            newDirection = MotionDirection.WALK_DOWN_RIGHT;
        else if (x > 0)
            newDirection = MotionDirection.WALK_RIGHT;
        else if (x < 0)
            newDirection = MotionDirection.WALK_LEFT;
        else if (y < 0)
            newDirection = MotionDirection.WALK_DOWN;
        else if (y > 0)
            newDirection = MotionDirection.WALK_UP;
        else if (lastWalkedUp)
            newDirection = MotionDirection.IDLE_REAR;
        else newDirection = MotionDirection.IDLE_FRONT;

        if (newDirection == lastDirection)
            return;
        else if (x > 0 && y > 0)
            charAnimController.WalkUpRight();
        else if (x < 0 && y > 0)
            charAnimController.WalkUpLeft();
        else if (x < 0 && y < 0)
            charAnimController.WalkDownLeft();
        else if (x > 0 && y < 0)
            charAnimController.WalkDownRight();
        else if (x > 0)
            charAnimController.WalkRight();
        else if (x < 0)
            charAnimController.WalkLeft();
        else if (y < 0)
            charAnimController.WalkDown();
        else if (y > 0)
            charAnimController.WalkUp();
        else if (lastWalkedUp)
            charAnimController.RearIdle();
        else charAnimController.FrontIdle();

        lastDirection = newDirection;
    }
}
